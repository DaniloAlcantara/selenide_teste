package testes;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.testng.ScreenShooter;
import common.BaseTest;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.executeJavaScript;


public class Login extends BaseTest {

    @DataProvider(name = "login-messages")
    public Object[][] loginProvider() {
        return new Object[][]{
                {"474462031", "1234", "Não confere Login e Senha", "senha_incorreta"},//senha  incorreta
                {"123456789", "13949856dp*", "Não confere Login e Senha", "User_incorreto"}, //User incorreto
                {"", "13949856dp*", "Forneça o usuário", "User_vazio"}, //User vazio
                {"474462031", "", "Não confere Login e Senha", "senha_vazia"}, //senha vazia
        };
    }





    @Test(priority = 0)
    public void LoginSucesso() {

        login
                .open()
                .with("474462031", "13949856dp*")
                .username().shouldHave(text("DANILO PEDRO DE ALCANTARA -"));

        //assertEquals("DANILO PEDRO DE ALCANTARA -", "DANILO PEDRO DE ALCANTARA -");
        //$("#span_MPW0041vPRO_PESSOALNOME").shouldHave(text("DANILO PEDRO DE ALCANTARA -"));
        //screenshot("home_Siga");

    }


    //DDT (Data Driven Testing)
    @Test(dataProvider = "login-messages", priority = 1)
    public void DevoVerMsgsDeAlerta(String user, String senha, String ExpectMsg, String Print) {

        login
                .open()
                .with(user, senha)
                .message().shouldHave(text(ExpectMsg));

        //assertEquals(ExpectMsg, ExpectMsg);
        //screenshot(Print);

    }


   /* @AfterMethod
    public void tearDown() {
        login.clearSession();

    }*/
}
