package common;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.testng.ScreenShooter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import pages.LoginPage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Properties;

import static com.codeborne.selenide.Selenide.screenshot;
import static io.qameta.allure.Allure.addAttachment;

public class BaseTest {

    protected static LoginPage login;

    @BeforeMethod
    public void setup() {

        //rodar usanddo arquivo properties
        /*Properties prop = new Properties();
        InputStream inputFile = getClass().getClassLoader().getResourceAsStream("config.properties");

        try {
            prop.load(inputFile);
        } catch (IOException e) {
            System.out.println("problema com o arquivo de properties");
            e.printStackTrace();
        }
        ScreenShooter.captureSuccessfulTests = true;
        Configuration.headless = true;
        Configuration.browser = prop.getProperty("browser");
        Configuration.startMaximized = true;
        Configuration.baseUrl = prop.getProperty("url");
        Configuration.timeout = Long.parseLong(prop.getProperty("timeout"));*/

        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
        Configuration.baseUrl = "https://siga.cps.sp.gov.br/aluno/";
        Configuration.timeout = 30000;

        login = new LoginPage();
    }

    @AfterMethod
    public void finish() {
        //scrreeshot pelo selenide
        String temp_print = screenshot("temp_print");

        //transformando em binario para anexar no allure
        try {
            BufferedImage bimage = ImageIO.read(new File(temp_print));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bimage, "png", baos);
            byte[] final_print = baos.toByteArray();
            addAttachment("Evidência", new ByteArrayInputStream(final_print));

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Erro ao anexar screenshot no relatorio" + e.getMessage());
        }


        login.clearSession();
    }
}
