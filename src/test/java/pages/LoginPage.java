package pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.executeJavaScript;

public class LoginPage {

    public LoginPage open(){
        Selenide.open("/login.aspx");
        return this;
    }

    public LoginPage with(String user, String senha){

        $("#vSIS_USUARIOID").setValue(user);
        $("#vSIS_USUARIOSENHA").setValue(senha);
        $(".Button").click();

        return this;
    }

    public SelenideElement username(){
        return $("#span_MPW0041vPRO_PESSOALNOME");
    }

    public SelenideElement message(){
        return $("text");
    }

    public void clearSession(){
        executeJavaScript("localStorage.clear();");
    }

}
